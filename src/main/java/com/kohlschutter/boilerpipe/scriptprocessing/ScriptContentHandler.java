package com.kohlschutter.boilerpipe.scriptprocessing;

import com.kohlschutter.boilerpipe.document.TextBlock;
import com.kohlschutter.boilerpipe.util.UnicodeTokenizer;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.regex.Pattern;

public class ScriptContentHandler {
    static final String ANCHOR_TEXT_START = "$\ue00a<";
    static final String ANCHOR_TEXT_END = ">\ue00a$";

    StringBuilder tokenBuffer = new StringBuilder();
    StringBuilder textBuffer = new StringBuilder();
    private final List<TextBlock> textBlocks = new ArrayList<TextBlock>();
    boolean inAnchorText = false;
    private BitSet currentContainedTextElements = new BitSet();
    private int offsetBlocks = 0;

    public ScriptContentHandler(ArrayList<String> content) {
        for(String c:content){
            flushBlock(c);
        }
    }

    public void flushBlock(String text) {
        tokenBuffer=new StringBuilder(text);
        textBuffer=new StringBuilder(text);
        final int length = tokenBuffer.length();

        final String[] tokens = UnicodeTokenizer.tokenize(tokenBuffer);
        int numWords = 0;
        int numLinkedWords = 0;
        int numWrappedLines = 0;
        int currentLineLength = -1;
        final int maxLineLength = 80;
        int numTokens = 0;
        int numWordsCurrentLine = 0;

        for (String token : tokens) {
            if (ANCHOR_TEXT_START.equals(token)) {
                inAnchorText = true;
            } else if (ANCHOR_TEXT_END.equals(token)) {
                inAnchorText = false;
            } else if (isWord(token)) {
                numTokens++;
                numWords++;
                numWordsCurrentLine++;
                if (inAnchorText) {
                    numLinkedWords++;
                }
                final int tokenLength = token.length();
                currentLineLength += tokenLength + 1;
                if (currentLineLength > maxLineLength) {
                    numWrappedLines++;
                    currentLineLength = tokenLength;
                    numWordsCurrentLine = 1;
                }
            } else {
                numTokens++;
            }
        }
        if (numTokens == 0) {
            return;
        }
        int numWordsInWrappedLines;
        if (numWrappedLines == 0) {
            numWordsInWrappedLines = numWords;
            numWrappedLines = 1;
        } else {
            numWordsInWrappedLines = numWords - numWordsCurrentLine;
        }

        TextBlock tb =
                new TextBlock(textBuffer.toString().trim(), currentContainedTextElements, numWords,
                        numLinkedWords, numWordsInWrappedLines, numWrappedLines, offsetBlocks);
        currentContainedTextElements = new BitSet();

        offsetBlocks++;

        textBuffer.setLength(0);
        tokenBuffer.setLength(0);

        textBlocks.add(tb);
    }
    public List<TextBlock> getTextBlocks()
    {
        return textBlocks;
    }
    private static final Pattern PAT_VALID_WORD_CHARACTER = Pattern
            .compile("[\\p{L}\\p{Nd}\\p{Nl}\\p{No}]");

    private static boolean isWord(final String token) {
        return PAT_VALID_WORD_CHARACTER.matcher(token).find();
    }
}