package com.kohlschutter.boilerpipe.scriptprocessing;

import com.kohlschutter.boilerpipe.document.TextBlock;
import com.kohlschutter.boilerpipe.document.TextDocument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class ScriptProcessing {
    public static TextDocument process(String inputstring)
    {
        String[] str=inputstring.split("\\{|\\}|<");//split by { or }

        ArrayList<String> output=new ArrayList<String>();
        for(String s:str){
            if(!s.equals(""))
            {
                String[] list=s.split("\"");
                for(String i:list) {
                    i=i.trim();
                    if(!i.equals(",")&&!i.equals(":")&&!i.equals(""))
                    {
                        i=i.replaceAll("[\\x5C,\\x2F]", "");
                        i=i.replaceAll("\n"," ");
                        if(i.contains("u003")) {
                            for(String c:i.split("u003c|u003e")) {
                                if(c.split(" ").length>2) output.add(c);
                            }
                        }
                    }
                }
            }
        }
        ScriptContentHandler contentHandler=new ScriptContentHandler(output);
        List<TextBlock> textBlocks=contentHandler.getTextBlocks();
        TextDocument textDocument=new TextDocument(textBlocks) ;
        return textDocument;
    }
}
