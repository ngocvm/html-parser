package demoMain;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.document.TextBlock;
import com.kohlschutter.boilerpipe.document.TextDocument;
import com.kohlschutter.boilerpipe.extractors.CommonExtractors;
import com.kohlschutter.boilerpipe.sax.BoilerpipeSAXInput;
import com.kohlschutter.boilerpipe.sax.HTMLDocument;
import com.kohlschutter.boilerpipe.sax.HTMLFetcher;
import com.kohlschutter.boilerpipe.scriptprocessing.ScriptContentHandler;
import com.kohlschutter.boilerpipe.scriptprocessing.ScriptProcessing;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import sun.font.Script;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws BoilerpipeProcessingException, SAXException, IOException {
        String[] url=new String[10];
        try{
            Gson gson=new Gson();
            JsonReader reader=new JsonReader(new FileReader("Testdata/data.json"));
            List<String> data=gson.fromJson(reader,new TypeToken<List<String>>() {
            }.getType());
            int size=data.size();
            int cnt=0;
            Map<String,String> map=new HashMap<String, String>();
            for(String link:data)
            {
                String content=getdata(link);
                map.put(link,content);
                if(content.split("\n").length>15){
                    System.out.println(link+"  1");
                    cnt++;
                }
                else System.out.println(link+" 0");
            }
            System.out.println(cnt+"/"+size);
            gson.toJson(map,new FileWriter("result.json"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }


    }
    public static String getdata(String link) throws SAXException, IOException, BoilerpipeProcessingException {
        final HTMLDocument htmlDoc = HTMLFetcher.fetch(new URL(link));
        final TextDocument doc = new BoilerpipeSAXInput(htmlDoc.toInputSource()).getTextDocument();
        String content = CommonExtractors.ARTICLE_EXTRACTOR.getText(doc);
        return content;
    }
}

