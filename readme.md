### Chức năng

- Bản forced từ project https://github.com/kohlschutter/boilerpipe
- Chức năng: tự động trích nội dung của một html doc.

### Cách sử dụng

```java
import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.document.TextDocument;
import com.kohlschutter.boilerpipe.extractors.CommonExtractors;
import com.kohlschutter.boilerpipe.sax.BoilerpipeSAXInput;
import com.kohlschutter.boilerpipe.sax.HTMLDocument;
import com.kohlschutter.boilerpipe.sax.HTMLFetcher;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws BoilerpipeProcessingException, SAXException, IOException {
        String url2 = "https://www.yes24.vn/ly-giu-nhiet-locklock-clip-tumbler-540ml-lhc4151blk-mau-den-p1948905.html?cidevent=bestseller";
        final HTMLDocument htmlDoc = HTMLFetcher.fetch(new URL(url2));
        final TextDocument doc = new BoilerpipeSAXInput(htmlDoc.toInputSource()).getTextDocument();
        String content = CommonExtractors.ARTICLE_EXTRACTOR.getText(doc);
        System.out.println(content);
        
        String title = doc.getTitle();
	System.out.println(title);
    }
}
```


